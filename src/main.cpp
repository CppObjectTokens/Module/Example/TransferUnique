/*
 * Copyright (c) 2018-2020 Viktor Kireev
 * Distributed under the MIT License
 */

#include <otn/all.hpp>

#include <memory>
#include <thread>
#include <iostream>

#define THREAD_ID "[" << std::this_thread::get_id() << "] "

struct Engine
{
    Engine(std::string model) : model{model} {}

    Engine(const Engine& other) = delete;
    Engine&     operator=(const Engine& other) = delete;

    std::string model;
};

class StdCar
{
public:
    StdCar(std::unique_ptr<Engine> engine)
        : m_engine{std::move(engine)}
    {}

    void replaceEngine(std::unique_ptr<Engine> engine)
    {
        using namespace std;

        string old_model = (*m_engine).model;
        m_engine = move(engine);
        cout << THREAD_ID << "StdCar replace engine " << old_model
             << " -> " << (*m_engine).model
             << endl;
    }

private:
    std::unique_ptr<Engine> m_engine;
};

class UplCar
{
public:
    UplCar(otn::unique<Engine> engine)
        : m_engine{std::move(engine)}
    {}

    void replaceEngine(otn::unique<Engine> engine)
    {
        using namespace std;

        string old_model = (*m_engine).model;
        m_engine = move(engine);
        cout << THREAD_ID << "UplCar replace engine " << old_model
             << " -> " << (*m_engine).model
             << endl;
    }

private:
    otn::unique<Engine> m_engine;
};

std::thread lambdaTransfer(std::shared_ptr<StdCar> car)
{
    using namespace std;

    cout << THREAD_ID << "Std lambda transfer" << endl;

    auto new_engine = make_unique<Engine>("V-8 Lambda");

    auto replace_engine =
        [engine = move(new_engine), car]() mutable {
            this_thread::sleep_for(chrono::milliseconds(200));
            (*car).replaceEngine(move(engine));
        };

    return thread{move(replace_engine)};
}

std::thread lambdaTransfer(otn::shared<UplCar> car)
{
    using namespace std;
    using namespace otn;

    cout << THREAD_ID << "Upl lambda transfer" << endl;

    otn::unique<Engine> new_engine{itself, "V-8 Lambda"};

    auto replace_engine =
        [engine = move(new_engine), car]() mutable {
            this_thread::sleep_for(chrono::milliseconds(400));
            (*car).replaceEngine(move(engine));
        };

    return thread{move(replace_engine)};
}

std::thread functionLambdaTransfer(std::shared_ptr<StdCar> car [[maybe_unused]])
{
    using namespace std;

    cout << THREAD_ID << "Std function lambda transfer" << endl;

    auto new_engine = make_unique<Engine>("V-8 Function Lambda");

    #if 0
    // Error
    function<void()> replace_engine =
        [engine = move(new_engine), car]() mutable {
            this_thread::sleep_for(chrono::milliseconds(600));
            (*car).replaceEngine(move(engine));
        };
    #else
    function<void()> replace_engine =
        [] {
            this_thread::sleep_for(chrono::milliseconds(600));
            cout << THREAD_ID << "Unable std function lambda transfer" << endl;
        };
    #endif

    return thread{move(replace_engine)};
}

std::thread functionLambdaTransfer(otn::shared<UplCar> car)
{
    using namespace std;
    using namespace otn;

    cout << THREAD_ID << "Upl function lambda transfer" << endl;

    otn::unique<Engine> new_engine{itself, "V-8 Function Lambda"};

    // Use an otn::unique_carrier to transfer the unique new_engine.
    function<void()> replace_engine =
        [engine = unique_carrier{move(new_engine)}, car]() mutable {
            this_thread::sleep_for(chrono::milliseconds(800));
            (*car).replaceEngine(move(engine));
        };

    return thread{move(replace_engine)};
}

std::thread functionBindTransfer(std::shared_ptr<StdCar> car [[maybe_unused]])
{
    using namespace std;

    cout << THREAD_ID << "Std function bind transfer" << endl;

    auto new_engine = make_unique<Engine>("V-8 Function Bind");

    #if 0
    // Error
    function<void()> replace_engine =
        std::bind(&StdCar::replaceEngine, car,
                  move(new_engine));
    #else
    function<void()> replace_engine =
        [] {
            this_thread::sleep_for(chrono::milliseconds(1000));
            cout << THREAD_ID << "Unable std function bind transfer" << endl;
        };
    #endif

    return thread{move(replace_engine)};
}

std::thread functionBindTransfer(otn::shared<UplCar> car)
{
    using namespace std;
    using namespace otn;

    cout << THREAD_ID << "Upl function bind transfer" << endl;

    otn::unique<Engine> new_engine{itself, "V-8 Function Bind"};

    function<void()> replace_engine =
        std::bind(&UplCar::replaceEngine, car,
                  unique_carrier(move(new_engine)));

    return thread{move(replace_engine)};
}

int main()
{
    using namespace std;
    using namespace otn;

    auto std_car = std::make_shared<StdCar>(std::make_unique<Engine>("V-6 Init"));
    (*std_car).replaceEngine(std::make_unique<Engine>("V-6 Direct"));

    shared<UplCar> otn_car{itself, otn::unique<Engine>{itself, "V-6 Init"}};
    (*otn_car).replaceEngine(otn::unique<Engine>{itself, "V-6 Direct"});

    vector<thread> threads;
    threads.push_back(lambdaTransfer(std_car));
    threads.push_back(lambdaTransfer(otn_car));
    threads.push_back(functionLambdaTransfer(std_car));
    threads.push_back(functionLambdaTransfer(otn_car));
    threads.push_back(functionBindTransfer(std_car));
    threads.push_back(functionBindTransfer(otn_car));

    for (auto& thread:threads)
        thread.join();

    return 0;
}
